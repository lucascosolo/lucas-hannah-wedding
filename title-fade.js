// Function to animate the title
function animateTitle() {
    const titleElement = document.getElementById('animated-title');
    
    // Fade out the current title
    titleElement.style.opacity = 0;

    // Add a delay before changing the title and fading back in
    setTimeout(() => {
        // Change the title
        titleElement.innerHTML = 'Hannah and Lucas Cosolo';

        // Fade in the new title
        titleElement.style.opacity = 1;

        // Remove the class for hidden effect
        titleElement.classList.remove('hidden');
    }, 1000); // Adjust the delay as needed
}

// Trigger the animation after a delay (adjust the delay as needed)
setTimeout(animateTitle, 2000); // 2000 milliseconds (2 seconds) delay